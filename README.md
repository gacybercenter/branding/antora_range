# Cyber Range Antora Theme

This project is a modification to the default Antora UI theme to follow branding guidelines.

## Cascading Style Sheets (CSS)

```
src
├── img
|   └── gcc_vert_white.svg
└── partials
    ├── head-icons.hbs
    └── header-content.hbs
```